import re
import subprocess

from cki_lib.logger import get_logger

# TODO make triggers installable and reuse the code from there


def main(project, pipeline):
    """
    Cancel a pipeline, canceling the beaker job as well.
    """
    get_logger(__name__).info('Cancelling pipeline %s', pipeline.id)
    pipeline.cancel()
    for job in pipeline.jobs.list():
        if job.stage == 'test':
            job = project.jobs.get(job.id)
            for beaker_job_id in re.findall('J:[0-9]+', job.trace().decode()):
                args = ['bkr', 'job-cancel', beaker_job_id]
                subprocess.check_call(args)
