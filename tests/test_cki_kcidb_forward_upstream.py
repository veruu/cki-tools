"""Test the kcidb forwarder."""
import copy
import unittest
from unittest import mock

from cki.kcidb import forward_upstream


class TestKCIDBForwarder(unittest.TestCase):
    """Test the kcidb forwarder."""

    def test_callback_submit(self):
        """Test that messages are submitted."""
        message = mock.Mock()
        forward_upstream.callback(message, None, None)
        self.assertTrue(message.submit.called)
        self.assertFalse(message.add.called)

    def test_callback_add(self):
        """Test that messages are added to the list."""
        message_ok = {
            'object_type': 'test',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'foo': 'bar',
                'misc': {
                    'is_public': True
                }
            }
        }
        message = mock.Mock()
        ack_fn = mock.Mock()

        # Message has status != new
        message_not_new = copy.deepcopy(message_ok)
        message_not_new['status'] = 'foobar'
        forward_upstream.callback(message, message_not_new, ack_fn)
        self.assertFalse(message.add.called)
        self.assertTrue(ack_fn.called)
        ack_fn.reset_mock()

        # Message has is_public == False
        message_private = copy.deepcopy(message_ok)
        message_private['object']['misc']['is_public'] = False
        forward_upstream.callback(message, message_private, ack_fn)
        self.assertFalse(message.add.called)
        self.assertTrue(ack_fn.called)
        ack_fn.reset_mock()

        # Message is ok
        forward_upstream.callback(message, message_ok, ack_fn)
        message.add.assert_called_with(
            'test', {'foo': 'bar', 'misc': {'is_public': True}}, ack_fn)
        self.assertTrue(message.add.called)
        self.assertFalse(message.submit.called)
        # This message is acked when submitted.
        self.assertFalse(ack_fn.called)

    def test_kcidbmessage_add_revision(self):
        """Test KCIDBMessage add revision."""
        ack_fn = mock.Mock()
        message = forward_upstream.KCIDBMessage(None)
        self.assertEqual(message.revisions, [])

        message.add('revision', {'foo': 'bar'}, ack_fn)
        self.assertEqual(message.revisions, [{'foo': 'bar'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn])

        message.add('revision', {'bar': 'foo'}, ack_fn)
        self.assertEqual(message.revisions, [{'foo': 'bar'}, {'bar': 'foo'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_build(self):
        """Test KCIDBMessage add build."""
        ack_fn = mock.Mock()
        message = forward_upstream.KCIDBMessage(None)
        self.assertEqual(message.builds, [])

        message.add('build', {'foo': 'bar'}, ack_fn)
        self.assertEqual(message.builds, [{'foo': 'bar'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn])

        message.add('build', {'bar': 'foo'}, ack_fn)
        self.assertEqual(message.builds, [{'foo': 'bar'}, {'bar': 'foo'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_test(self):
        """Test KCIDBMessage add test."""
        ack_fn = mock.Mock()
        message = forward_upstream.KCIDBMessage(None)
        self.assertEqual(message.tests, [])

        message.add('test', {'foo': 'bar'}, ack_fn)
        self.assertEqual(message.tests, [{'foo': 'bar'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn])

        message.add('test', {'bar': 'foo'}, ack_fn)
        self.assertEqual(message.tests, [{'foo': 'bar'}, {'bar': 'foo'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_other(self):
        """Test KCIDBMessage add unhandled."""
        message = forward_upstream.KCIDBMessage(None)
        self.assertEqual(message.tests, [])

        self.assertRaises(KeyError, message.add, 'foo',
                          {'foo': 'bar'}, mock.Mock())

    def test_kcidbmessage_encoded(self):
        """Test KCIDBMessage encoded property."""
        revision = {'origin': 'test',
                    'id': '92b8f402aa964f209772e30190af5de818af996c'}
        build = {'origin': 'test',
                 'revision_id': '92b8f402aa964f209772e30190af5de818af996c',
                 'id': 'test:1'}
        test = {'origin': 'test', 'build_id': 'test:1', 'id': 'test:1'}

        message = forward_upstream.KCIDBMessage(None)
        message.version = {'major': 3, 'minor': 0}
        message.add('revision', revision, None)
        message.add('build', build, None)
        message.add('test', test, None)

        self.assertDictEqual(
            {
                'version': {'major': 3, 'minor': 0},
                'revisions': [revision],
                'builds': [build],
                'tests': [test],
            }, message.encoded
        )

    @mock.patch('cki.kcidb.forward_upstream.kcidb.io.schema.validate')
    def test_kcidbmessage_encoded_validated(self, validate):
        """Test kcidb validate_exactly is called."""
        rev, build, test = {'foo': 'foo'}, {'bar': 'bar'}, {'foo': 'bar'}
        expected_msg = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [rev], 'builds': [build], 'tests': [test]
        }

        message = forward_upstream.KCIDBMessage(None)
        message.version = {'major': 3, 'minor': 0}
        message.revisions.append(rev)
        message.builds.append(build)
        message.tests.append(test)

        self.assertDictEqual(expected_msg, message.encoded)
        validate.assert_called_with(expected_msg)

    def test_kcidbmessage_has_objects(self):
        """Test KCIDBMessage has_objects property."""
        message = forward_upstream.KCIDBMessage(None)
        self.assertFalse(message.has_objects)

        for prop in (message.revisions, message.builds, message.tests):
            prop.append({'foo': 'bar'})
            self.assertTrue(message.has_objects)
            prop.remove({'foo': 'bar'})
            self.assertFalse(message.has_objects)

    def test_kcidbmessage_len(self):
        """Test KCIDBMessage lenght property."""
        message = forward_upstream.KCIDBMessage(None)
        self.assertEqual(0, len(message))

        message.revisions = [{}]
        message.builds = [{}, {}]
        message.tests = [{}, {}, {}]
        self.assertEqual(6, len(message))

    def test_kcidbmessage_clear(self):
        """Test KCIDBMessage clear method."""
        rev, build, test = {'foo': 'foo'}, {'bar': 'bar'}, {'foo': 'bar'}

        message = forward_upstream.KCIDBMessage(None)
        message.version = {'major': 3, 'minor': 0}
        message.revisions.append(rev)
        message.builds.append(build)
        message.tests.append(test)
        message.msg_ack_callbacks.append(mock.Mock())

        properties = (
            'revisions',
            'builds',
            'tests',
            'msg_ack_callbacks',
        )

        for prop_name in properties:
            prop = getattr(message, prop_name)
            self.assertEqual(1, len(prop), prop_name)

        message.clear()

        for prop_name in properties:
            prop = getattr(message, prop_name)
            self.assertEqual(0, len(prop), prop_name)
        self.assertIsNone(message.version)

    def test_kcidbmessage_submit(self):
        """Test KCIDBMessage submit method."""
        kcidb_mock = mock.Mock()
        message = forward_upstream.KCIDBMessage(kcidb_mock)
        message.version = {'major': 3, 'minor': 0}
        message.clear = mock.Mock()
        message.ack_messages = mock.Mock()

        message.submit()
        kcidb_mock.submit.assert_called_with(message.encoded)
        self.assertTrue(message.clear.called)
        self.assertTrue(message.ack_messages.called)

    def test_kcidbmessage_submit_dryrun(self):
        """Test KCIDBMessage submit method."""
        kcidb_mock = mock.Mock()
        message = forward_upstream.KCIDBMessage(kcidb_mock, dry_run=True)
        message.clear = mock.Mock()
        message.ack_messages = mock.Mock()

        message.submit()
        self.assertFalse(kcidb_mock.submit.called)
        self.assertTrue(message.clear.called)
        self.assertTrue(message.ack_messages.called)

    def test_kcidbmessage_add_set_version(self):
        """Test KCIDBMessage add sets message version."""
        message = forward_upstream.KCIDBMessage(None)

        message.add(
            'revision', {'misc': {'kcidb': {
                'version': {'major': 3, 'minor': 0}}}}, None)
        self.assertDictEqual(
            {'major': 3, 'minor': 0},
            message.version,
        )

        # New message does not replace it.
        message.add(
            'revision', {'misc': {'kcidb': {
                'version': {'major': 4, 'minor': 1}}}}, None)
        self.assertDictEqual(
            {'major': 3, 'minor': 0},
            message.version,
        )

        # New message replaces it only after clear.
        message.clear()
        message.add(
            'revision', {'misc': {'kcidb': {
                'version': {'major': 4, 'minor': 1}}}}, None)
        self.assertDictEqual(
            {'major': 4, 'minor': 1},
            message.version,
        )

    def test_ack_messages(self):
        """Test ack_messages does what is expected to do."""
        message = forward_upstream.KCIDBMessage(None)
        message.msg_ack_callbacks = [
            mock.Mock(),
            mock.Mock()
        ]
        message.ack_messages()
        for ack_fn in message.msg_ack_callbacks:
            self.assertTrue(ack_fn.called)

    def test_add_remove_misc(self):
        """Test that the some keys are removed from misc."""
        message = forward_upstream.KCIDBMessage(None)
        revisions = [
            {'foo': 'bar', 'misc': {'bar': 'bar', 'kcidb': {'foo': 'foo'}}},
            {'foo': 'bar', 'misc': {'bar': 'bar', 'is_public': True}},
            {'foo': 'bar', 'misc': {
                'bar': 'bar', 'is_public': False, 'kcidb': {'foo': 'foo'}}},
        ]

        for rev in revisions:
            message.add('revision', rev, None)

        self.assertListEqual(
            [
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
            ],
            message.revisions
        )
