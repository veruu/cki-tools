"""Test cki.deployment_tools.render_jinja2 module."""
import pathlib
import tempfile
import unittest
from unittest import mock

from cki.deployment_tools import render_jinja2


class TestMonitRender(unittest.TestCase):
    """Test render_jinja2 methods."""

    def setUp(self):
        """Create temp files."""
        self.temp_files = [
            tempfile.NamedTemporaryFile() for _ in range(3)
        ]

    def tearDown(self):
        """Delete temp files."""
        for file in self.temp_files:
            file.close()

    @mock.patch.dict(
        'cki.deployment_tools.render_jinja2.os.environ', {'FOO': 'BAR'}
    )
    def test_from_env(self):
        """Test from_env filter."""
        cases = [
            ('$FOO', 'BAR'),
            ('${FOO}', 'BAR'),
            ('${FOO', '${FOO'),
            ('$FOO}', 'BAR}'),
            ('{FOO}', '{FOO}'),
            ('#FOO', '#FOO'),
        ]

        for variable, value in cases:
            self.assertEqual(
                value,
                render_jinja2.from_env(None, variable)
            )

        self.assertRaises(
            KeyError, render_jinja2.from_env, None, '$NOT_DEFINED_VAR'
        )

    @mock.patch.dict(
        'cki.deployment_tools.render_jinja2.os.environ', {'FOOBAR': 'BARBAR'}
    )
    def test_render(self):
        """
        Test render function.

        Fake template and data, and check that the output is as expected.
        """
        template = self.temp_files[0].name
        data = self.temp_files[1].name

        pathlib.Path(template).write_text(
            'foo\n{{ foo }}\n{{ bar }}\n{{ bar|env }}\n{{ env["FOOBAR"] }}'
        )
        pathlib.Path(data).write_text('foo: bar\nbar: $FOOBAR')

        rendered = render_jinja2.render(template, data)
        self.assertEqual('foo\nbar\n$FOOBAR\nBARBAR\nBARBAR', rendered)

    @mock.patch('cki.deployment_tools.render_jinja2.parse_args')
    @mock.patch('cki.deployment_tools.render_jinja2.render')
    def test_main(self, render, parse_args):
        """Test main function."""
        args = mock.Mock()
        args.data = 'data'
        args.template = 'template'
        args.output = self.temp_files[0].name
        parse_args.return_value = args
        render.return_value = 'rendered\ntemplate'

        render_jinja2.main()

        render.assert_called_with('template', 'data')
        self.assertEqual(
            'rendered\ntemplate',
            pathlib.Path(args.output).read_text()
        )
