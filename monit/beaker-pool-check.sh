#!/bin/bash
set -euo pipefail

# Check the number of systems in a Beaker pool
#
# Usage: beaker-pool-check.sh POOL_NAME MIN_POOL_COUNT
#
# Required environment variables:
# - BEAKER_URL

POOL_JSON="$(curl -kLs -H "Accept: application/json" --retry 3 "${BEAKER_URL}/pools/$1/")"
SYSTEMS_IN_POOL="$(jq -c -r ".systems | length" <<< "${POOL_JSON}")"

echo "${SYSTEMS_IN_POOL} of at least $2 expected hosts"

test "${SYSTEMS_IN_POOL}" -ge "$2"
